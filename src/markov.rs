use std::collections::HashMap;
use std::fs;

use anyhow::Result;
use log::{info, warn};
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct OldMarkov {
    data: HashMap<String, Vec<String>>,
    key_size: u8,
    start_keys: Vec<String>,
    pub import_path: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Markov {
    data: HashMap<String, Vec<String>>,
    key_size: u8,
    start_keys: Vec<String>,
    save_path: String,
    should_save: bool,
}

impl Markov {
    pub fn save(&self) -> Result<()> {
        if self.should_save {
            fs::write(&self.save_path, rmp_serde::to_vec(self)?)?;
        }

        Ok(())
    }

    pub fn add_strings<T: AsRef<str>>(&mut self, strings: &[T]) {
        for string in strings {
            let words: Vec<&str> = string.as_ref().split_whitespace().collect();
            for i in 0..words.len().saturating_sub(self.key_size as usize) {
                let mut key = words[i].to_string();
                for word in words.iter().take(i + self.key_size as usize).skip(i + 1) {
                    key += " ";
                    key += word;
                }
                if i == 0 {
                    self.start_keys.push(key.clone());
                }
                let ngram = words[i + self.key_size as usize];
                if let Some(val) = self.data.get_mut(&key) {
                    val.push(ngram.to_owned());
                } else {
                    self.data.insert(key, vec![ngram.to_owned()]);
                }
            }
        }
    }

    pub fn generate_string(&self) -> String {
        const MAX_LEN: u16 = 1600;

        let mut key = self
            .start_keys
            .get(thread_rng().gen_range(0..self.start_keys.len()))
            .unwrap()
            .split(' ')
            .collect::<Vec<&str>>();

        let mut out: Vec<&str> = key.clone();

        while let Some(val) = self.data.get(&*key.join(" ")) {
            if out.len() > MAX_LEN as usize {
                break;
            }
            out.push(&val[thread_rng().gen_range(0..val.len())]);
            key = Vec::from(&out[out.len().saturating_sub(self.key_size as usize)..out.len()]);
        }

        out.join(" ")
    }
}

pub struct MarkovBuilder<'a> {
    data: MarkovData<'a>,
    key_size: u8,
    should_import: bool,
    should_save: bool,
    save_path: &'a str,
}

impl<'a> MarkovBuilder<'a> {
    pub fn new() -> Self {
        Self {
            data: MarkovData::File("message-dump.txt"),
            key_size: 2,
            should_import: true,
            should_save: true,
            save_path: "markov_data",
        }
    }

    #[allow(dead_code)]
    pub fn data(mut self, data: MarkovData<'a>) -> Self {
        self.data = data;
        self
    }

    #[allow(dead_code)]
    pub fn key_size(mut self, key_size: u8) -> Self {
        self.key_size = key_size;
        self
    }

    #[allow(dead_code)]
    pub fn should_import(mut self, should_import: bool) -> Self {
        self.should_import = should_import;
        self
    }

    #[allow(dead_code)]
    pub fn should_save(mut self, should_save: bool) -> Self {
        self.should_save = should_save;
        self
    }

    #[allow(dead_code)]
    pub fn save_path(mut self, save_path: &'a str) -> Self {
        self.save_path = save_path;
        self
    }

    pub fn build(self) -> Result<Markov> {
        let start_time = std::time::Instant::now();

        if self.should_import {
            if let Ok(val) = fs::read(self.save_path) {
                match rmp_serde::from_slice::<Markov>(&val) {
                    Ok(val) => {
                        info!(
                            "Markov data serialized in: {:?}",
                            std::time::Instant::now().duration_since(start_time)
                        );
                        if self.key_size != val.key_size {
                            warn!("Asked to construct a Markov instance with key_size {}, but imported one with {} instead.", self.key_size, val.key_size);
                        }
                        return Ok(val);
                    }
                    Err(err) => match rmp_serde::from_slice::<OldMarkov>(&val) {
                        Ok(val) => {
                            info!("migrating old markov data");
                            return Ok(Markov {
                                data: val.data,
                                key_size: val.key_size,
                                start_keys: val.start_keys,
                                save_path: val.import_path,
                                should_save: self.should_save,
                            });
                        }
                        _ => panic!("couldn't serialize found markov data: {err}"),
                    },
                }
            }
        }

        let mut markov = Markov {
            data: HashMap::new(),
            start_keys: Vec::new(),
            key_size: self.key_size,
            save_path: self.save_path.to_owned(),
            should_save: self.should_save,
        };

        match self.data {
            MarkovData::Slice(data) => markov.add_strings(data),
            MarkovData::File(path) => {
                let data = fs::read_to_string(path)?;
                markov.add_strings(&data.trim_end().split('\n').collect::<Vec<_>>())
            }
        };

        info!(
            "Markov data built in: {:?}",
            std::time::Instant::now().duration_since(start_time)
        );

        markov.save()?;

        Ok(markov)
    }
}

pub enum MarkovData<'a> {
    #[allow(dead_code)]
    Slice(&'a [&'a str]),
    File(&'a str),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generate() {
        let markov = MarkovBuilder::new()
            .should_import(false)
            .should_save(false)
            .data(MarkovData::Slice(&["hello world", "world is here"]))
            .key_size(1)
            .build()
            .unwrap();

        let generated = markov.generate_string();
        let word_count = generated.split_whitespace().count();
        assert!(word_count > 2 && word_count < 5);
        assert!(generated.contains("world"));
    }

    #[test]
    fn save() {
        const SAVE_PATH: &str = "__markov_data_test";

        let markov = MarkovBuilder::new()
            .save_path(SAVE_PATH)
            .should_import(false)
            .should_save(true)
            .data(MarkovData::Slice(&["hello world", "world is here"]))
            .key_size(1)
            .build()
            .unwrap();

        markov.save().unwrap();

        let imported_markov = MarkovBuilder::new()
            .save_path(SAVE_PATH)
            .should_import(true)
            .should_save(false)
            .key_size(1)
            .build()
            .unwrap();

        assert!(markov == imported_markov);

        fs::remove_file(SAVE_PATH).unwrap();
    }
}
