use anyhow::Result;
use serenity::all::{ChannelId, FullEvent};

use crate::FrameworkContext;

mod cache_ready;
mod guild_scheduled_event_create;
mod guild_scheduled_event_delete;
mod guild_scheduled_event_update;
mod interaction_create;
mod message;
mod ready;
mod voice_state_update;

const EVENT_REPORT_CHANNEL: ChannelId = ChannelId::new(924343631761006592);

pub async fn handle(ctx: FrameworkContext<'_>, event: &FullEvent) -> Result<()> {
    match event {
        FullEvent::Ready { data_about_bot } => ready::handle(ctx, data_about_bot).await,
        FullEvent::CacheReady { guilds } => cache_ready::handle(ctx, guilds).await,
        FullEvent::Message { new_message } => message::handle(ctx, new_message).await,
        FullEvent::GuildScheduledEventCreate { event } => {
            guild_scheduled_event_create::handle(ctx, event).await
        }
        FullEvent::GuildScheduledEventDelete { event } => {
            guild_scheduled_event_delete::handle(ctx, event).await
        }
        FullEvent::GuildScheduledEventUpdate { event } => {
            guild_scheduled_event_update::handle(ctx, event).await
        }
        FullEvent::InteractionCreate { interaction } => {
            interaction_create::handle(ctx, interaction).await
        }
        FullEvent::VoiceStateUpdate { old, new } => voice_state_update::handle(ctx, old, new).await,
        _ => Ok(()),
    }
}
