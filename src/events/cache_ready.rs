use std::sync::atomic::Ordering;
use std::time::Duration;

use anyhow::{Error, Result};
use log::error;
use serenity::all::CreateMessage;
use serenity::futures::StreamExt;
use serenity::model::id::{ChannelId, GuildId};

use crate::FrameworkContext;

const MARKOV_LOOP_SECS: u64 = 3600;
const MARKOV_CHANNEL: ChannelId = ChannelId::new(464502359372857355);
const LAST_MESSAGES_TO_CHECK_N: usize = 10;

pub async fn handle(framework_ctx: FrameworkContext<'_>, _guilds: &[GuildId]) -> Result<()> {
    let ctx = framework_ctx.serenity_context.clone();
    let data = framework_ctx.user_data().clone();

    if !data.markov_loop_running.load(Ordering::Acquire) {
        let data_outer = data.clone();

        tokio::spawn(async move {
            loop {
                tokio::time::sleep(Duration::from_secs(MARKOV_LOOP_SECS)).await;
                {
                    let bot_user_id = ctx.cache.current_user().id;
                    if MARKOV_CHANNEL
                        .messages_iter(&ctx)
                        .take(LAST_MESSAGES_TO_CHECK_N)
                        .filter_map(|m| async { m.ok() })
                        .all(|m| async move { m.author.id != bot_user_id })
                        .await
                    {
                        let markov = data.markov.read().await;
                        for _ in 0..10 {
                            let generated_message = markov.generate_string();
                            if generated_message.len() < 2000 {
                                if let Err(e) =
                                    MARKOV_CHANNEL.say(&ctx.http, generated_message).await
                                {
                                    error!("error sending markov message: {}", e);
                                }
                                break;
                            }
                        }
                    }

                    {
                        let mut markov_queue = data.markov_message_queue.lock().await;
                        if !markov_queue.is_empty() {
                            #[cfg(debug_assertions)]
                            ::log::trace!("markov message queue not empty, saving");
                            {
                                let mut markov = data.markov.write().await;
                                markov.add_strings(&markov_queue);
                                markov.save()?;
                            }
                            *markov_queue = Vec::new();
                        }
                    }
                }
            }

            #[allow(unreachable_code)]
            Ok::<(), Error>(())
        });

        data_outer
            .markov_loop_running
            .store(true, Ordering::Release);
    }

    Ok(())
}
