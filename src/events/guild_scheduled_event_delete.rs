use anyhow::Result;
use serenity::all::CreateMessage;
use serenity::model::guild::ScheduledEvent;

use super::EVENT_REPORT_CHANNEL;
use crate::FrameworkContext;

pub async fn handle(framework_ctx: FrameworkContext<'_>, event: &ScheduledEvent) -> Result<()> {
    let ctx = framework_ctx.serenity_context;

    EVENT_REPORT_CHANNEL
        .send_message(
            &ctx.http,
            CreateMessage::new().content(
                String::from("Event **")
                    + &event.name
                    + "** was **cancelled** (previously scheduled for <t:"
                    + &event.start_time.unix_timestamp().to_string()
                    + ">).",
            ),
        )
        .await?;

    Ok(())
}
