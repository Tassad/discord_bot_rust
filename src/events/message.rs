use anyhow::Result;
use serenity::model::channel::Message;

use crate::FrameworkContext;

pub async fn handle(framework_ctx: FrameworkContext<'_>, new_message: &Message) -> Result<()> {
    let data = framework_ctx.user_data();
    let ctx = framework_ctx.serenity_context;

    if new_message.mentions_me(&ctx).await? {
        let markov = data.markov.read().await;
        loop {
            let reply = markov.generate_string();
            if reply.len() < 2000 {
                new_message.reply_ping(&ctx.http, reply).await?;
                break;
            }
        }
    } else if new_message.author.id != ctx.cache.current_user().id
        && !new_message.content.is_empty()
    {
        data.markov_message_queue
            .lock()
            .await
            .push(new_message.content.to_string());
        #[cfg(debug_assertions)]
        ::log::debug!(
            "markov message queue length: {}",
            data.markov_message_queue.lock().await.len()
        );
    }

    Ok(())
}
