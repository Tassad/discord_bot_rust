use std::env;
use std::ops::Deref;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

use anyhow::{Error, Result};
use log::error;
use poise::PrefixFrameworkOptions;
use reqwest::{Client as Reqwest, ClientBuilder as ReqwestBuilder};
use serenity::prelude::*;
use sqlx::postgres::PgPoolOptions;
use sqlx::PgPool;

use crate::error::handle_error;
use crate::markov::{Markov, MarkovBuilder};

mod commands;
mod error;
mod events;
mod markov;

#[derive(Debug)]
pub struct DataWrapper(Arc<Data>);
type Context<'a> = poise::Context<'a, Data, Error>;
type FrameworkContext<'a> = poise::FrameworkContext<'a, Data, Error>;

#[derive(Debug)]
pub struct Data {
    markov: Arc<RwLock<Markov>>,
    markov_loop_running: AtomicBool,
    markov_message_queue: Arc<Mutex<Vec<String>>>,
    reqwest: Reqwest,
    postgres: PgPool,
}

impl Deref for DataWrapper {
    type Target = Data;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();

    let token = env::var("DISCORD_TOKEN").expect("could not get discord token");

    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT
        | GatewayIntents::GUILDS
        | GatewayIntents::GUILD_SCHEDULED_EVENTS
        | GatewayIntents::GUILD_VOICE_STATES;

    let framework = poise::Framework::new(poise::FrameworkOptions {
        commands: commands::commands(),
        event_handler: |ctx, event| Box::pin(events::handle(ctx, event)),
        on_error: |e| {
            Box::pin(async move {
                if let Err(e) = handle_error(e).await {
                    error!("on_error: {:?}", e);
                }
            })
        },
        prefix_options: PrefixFrameworkOptions {
            prefix: Some("~".into()),
            mention_as_prefix: false,
            ..PrefixFrameworkOptions::default()
        },
        ..poise::FrameworkOptions::default()
    });

    let mut client = Client::builder(&token, intents)
        .framework(framework)
        .data(Arc::new(Data {
            markov: Arc::new(RwLock::new(MarkovBuilder::new().build()?)),
            markov_loop_running: AtomicBool::new(false),
            markov_message_queue: Arc::new(Mutex::new(Vec::new())),
            reqwest: ReqwestBuilder::new().pool_max_idle_per_host(1).build()?,
            postgres: PgPoolOptions::new()
                .connect(&env::var("DATABASE_URL")?)
                .await?,
        }))
        .await
        .expect("error creating client");

    if let Err(why) = client.start().await {
        error!("client error: {:?}", why);
    }

    Ok(())
}
