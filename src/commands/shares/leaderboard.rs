use std::io::Cursor;

use ab_glyph::{FontRef, PxScale};
use anyhow::Result;
use image::io::Reader;
use image::ImageFormat;
use image::{imageops::overlay, Rgba, RgbaImage};
use imageproc::{
    drawing::{draw_filled_rect_mut, draw_hollow_rect_mut, draw_text_mut, text_size},
    rect::Rect,
};
#[cfg(debug_assertions)]
use log::debug;
use reqwest::{Client, Url};

use crate::commands::shares::num_format;

const TITLE: &str = "Shares Leaderboard";
const SCALE: PxScale = PxScale { x: 72., y: 72. };
const MARGIN: f32 = SCALE.y / 4.;
const PADDING: i32 = (SCALE.y / 8.) as i32;

pub async fn generate_leaderboard(
    users: &[(String, String, f64, i32, i32)],
    reqwest: Client,
) -> Result<Vec<u8>> {
    let font_data: &[u8] = include_bytes!("../../../assets/NotoSansCJK-Medium.ttc");
    let font = FontRef::try_from_slice(font_data).unwrap();

    let mut img = RgbaImage::new(1920, 1080);

    for pixel in img.pixels_mut() {
        *pixel = Rgba([60, 60, 60, 255]);
    }

    draw_text_mut(
        &mut img,
        Rgba([255, 255, 255, 255]),
        centre_x(1920, SCALE, &font, TITLE),
        (MARGIN / 2.) as i32,
        SCALE,
        &font,
        TITLE,
    );

    let rank_rect = Rect::at((MARGIN * 4.) as i32, (SCALE.y + MARGIN) as i32).of_size(
        text_size(SCALE, &font, "10").0 + PADDING as u32 * 2,
        1080 - (SCALE.y * 2. + MARGIN) as u32,
    );
    draw_hollow_rect_mut(&mut img, rank_rect, Rgba([255, 255, 255, 255]));

    let prestige_width = text_size(SCALE, &font, "99").0 as i32;
    let prestige_rect = Rect::at(
        1920 - (MARGIN * 4.) as i32 - prestige_width,
        rank_rect.top(),
    )
    .of_size(
        prestige_width as u32 + PADDING as u32 * 2,
        rank_rect.height(),
    );
    draw_hollow_rect_mut(&mut img, prestige_rect, Rgba([255, 255, 255, 255]));

    let generators_rect = Rect::at(
        prestige_rect.left() - prestige_rect.width() as i32,
        rank_rect.top(),
    )
    .of_size(prestige_rect.width(), rank_rect.height());
    draw_hollow_rect_mut(&mut img, generators_rect, Rgba([255, 255, 255, 255]));

    let shares_width = text_size(SCALE, &font, "999 999 999,00").0 as i32;
    let shares_rect = Rect::at(
        generators_rect.left() - shares_width - PADDING * 2,
        rank_rect.top(),
    )
    .of_size(shares_width as u32 + PADDING as u32 * 2, rank_rect.height());
    draw_hollow_rect_mut(&mut img, shares_rect, Rgba([255, 255, 255, 255]));

    let name_rect = Rect::at(rank_rect.right(), rank_rect.top()).of_size(
        (shares_rect.left() - rank_rect.right()).try_into().unwrap(),
        rank_rect.height(),
    );
    draw_hollow_rect_mut(&mut img, name_rect, Rgba([255, 255, 255, 255]));

    let legend_row_rect = Rect::at(rank_rect.left(), rank_rect.top()).of_size(
        rank_rect.width()
            + prestige_rect.width()
            + generators_rect.width()
            + shares_rect.width()
            + name_rect.width(),
        rank_rect.height() / 11,
    );
    draw_filled_rect_mut(&mut img, legend_row_rect, Rgba([255, 255, 255, 255]));
    draw_text_mut(
        &mut img,
        Rgba([0, 0, 0, 255]),
        centre_x(rank_rect.width(), SCALE, &font, "#") + rank_rect.left(),
        legend_row_rect.top(),
        SCALE,
        &font,
        "#",
    );
    draw_text_mut(
        &mut img,
        Rgba([0, 0, 0, 255]),
        name_rect.left() + PADDING,
        legend_row_rect.top(),
        SCALE,
        &font,
        "User",
    );
    let prestige_img = image::open("assets/prestige_64px.png")
        .unwrap()
        .into_rgba8();
    overlay(
        &mut img,
        &prestige_img,
        (prestige_rect.left() + prestige_rect.width() as i32 / 2 - 64 / 2).into(),
        (legend_row_rect.top() + legend_row_rect.height() as i32 / 2 - 64 / 2).into(),
    );
    let factory_img = image::open("assets/factory_64px.png").unwrap().into_rgba8();
    overlay(
        &mut img,
        &factory_img,
        (generators_rect.left() + generators_rect.width() as i32 / 2 - 64 / 2).into(),
        (legend_row_rect.top() + legend_row_rect.height() as i32 / 2 - 64 / 2).into(),
    );
    let shares_img = image::open("assets/shares_64px.png").unwrap().into_rgba8();
    overlay(
        &mut img,
        &shares_img,
        (shares_rect.left() + shares_rect.width() as i32 / 2 - 64 / 2).into(),
        (legend_row_rect.top() + legend_row_rect.height() as i32 / 2 - 64 / 2).into(),
    );

    let mut prev_rect = legend_row_rect;
    for (i, (avatar_url, user, shares, generators, prestige_count)) in users.iter().enumerate() {
        let user_rect = Rect::at(prev_rect.left(), prev_rect.bottom())
            .of_size(prev_rect.width(), prev_rect.height());
        prev_rect = user_rect;

        let rank = (i + 1).to_string();
        draw_text_mut(
            &mut img,
            Rgba([255, 255, 255, 255]),
            centre_x(rank_rect.width(), SCALE, &font, &rank) + rank_rect.left(),
            centre_y(user_rect.height(), SCALE, &font, &rank) + user_rect.top(),
            SCALE,
            &font,
            &rank,
        );

        #[cfg(debug_assertions)]
        let start_time = std::time::Instant::now();

        let mut avatar_url = Url::parse(avatar_url)?;
        if avatar_url.query().is_some_and(|q| q.starts_with("size")) {
            avatar_url.set_query(Some("size=64"));
        }
        let mut avatar = Reader::new(Cursor::new(
            reqwest.get(avatar_url).send().await?.bytes().await?,
        ))
        .with_guessed_format()?
        .decode()?;
        if avatar.width() > 64 || avatar.height() > 64 {
            avatar = avatar.resize(64, 64, image::imageops::FilterType::Lanczos3);
        }

        #[cfg(debug_assertions)]
        debug!(
            "Got avatar for user {user} in: {:?}",
            std::time::Instant::now().duration_since(start_time)
        );

        overlay(
            &mut img,
            &avatar.to_rgba8(),
            (name_rect.left() + PADDING).into(),
            (user_rect.top() + user_rect.height() as i32 / 2 - 64 / 3).into(),
        );

        draw_text_mut(
            &mut img,
            Rgba([255, 255, 255, 255]),
            name_rect.left() + PADDING * 3 + 64,
            centre_y(user_rect.height(), SCALE, &font, user) + user_rect.top(),
            SCALE,
            &font,
            user,
        );

        let formatted_shares = num_format(*shares)?;
        draw_text_mut(
            &mut img,
            Rgba([255, 255, 255, 255]),
            shares_rect.right() - PADDING - text_size(SCALE, &font, &formatted_shares).0 as i32,
            centre_y(user_rect.height(), SCALE, &font, &shares.to_string()) + user_rect.top(),
            SCALE,
            &font,
            &formatted_shares,
        );

        let generator_string = generators.to_string();
        draw_text_mut(
            &mut img,
            Rgba([255, 255, 255, 255]),
            generators_rect.right() - PADDING - text_size(SCALE, &font, &generator_string).0 as i32,
            centre_y(user_rect.height(), SCALE, &font, &generator_string) + user_rect.top(),
            SCALE,
            &font,
            &generator_string,
        );

        let prestige_count_string = prestige_count.to_string();
        draw_text_mut(
            &mut img,
            Rgba([255, 255, 255, 255]),
            prestige_rect.right()
                - PADDING
                - text_size(SCALE, &font, &prestige_count_string).0 as i32,
            centre_y(user_rect.height(), SCALE, &font, &prestige_count_string) + user_rect.top(),
            SCALE,
            &font,
            &prestige_count_string,
        );
    }

    let mut buf = Vec::new();
    img.write_to(&mut Cursor::new(&mut buf), ImageFormat::WebP)?;
    Ok(buf)
}

fn centre_x(width: u32, scale: PxScale, font: &FontRef, text: &str) -> i32 {
    (width / 2 - text_size(scale, font, text).0 / 2) as i32
}

fn centre_y(height: u32, scale: PxScale, font: &FontRef, text: &str) -> i32 {
    (height / 2 - text_size(scale, font, text).1 / 2) as i32
}
