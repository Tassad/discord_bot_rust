use std::fmt::{Display, Formatter};

use anyhow::{Error, Result};
use log::{debug, error};
use poise::FrameworkError;

use crate::Data;

pub async fn handle_error(e: FrameworkError<'_, Data, Error>) -> Result<()> {
    match e {
        FrameworkError::Command { ctx, error, .. } => {
            if ctx.framework().options.commands.contains(ctx.command()) {
                let response = match error.downcast::<CommandError>() {
                    Ok(e) => e.to_string(),
                    Err(e) => {
                        debug!("{:?}", e);
                        "An error occurred while executing this command.".to_string()
                    }
                };
                if let Err(e) = ctx.say(response).await {
                    error!("could not reply in handle_error: {e}");
                };
            }
        }
        _ => error!("handle_error: {:?}", e),
    }

    Ok(())
}

#[derive(Debug)]
pub enum CommandError {
    GelbooruNoPosts,
    GelbooruPostTooLarge,
    AoNNoResults,
}

impl Display for CommandError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::GelbooruNoPosts => write!(f, "Gelbooru did not return any posts."),
            Self::GelbooruPostTooLarge => write!(f, "Found Gelbooru post size is above 25 MiB."),
            Self::AoNNoResults => write!(
                f,
                "Did not find any matching results from Archives of Nethys."
            ),
        }
    }
}

impl std::error::Error for CommandError {}
